﻿using EjercicioFinal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicioFinal
{
    class Area
    {
        private string categoria;
        private List<Empleados> empleados = new List<Empleados>();


        public Area(string categoria)
        {
            this.categoria = categoria;
        }

        public List<Empleados> agregarEmpleadosCategoria()
        {
            double cantidadEmpleados = empleadosContratar();
            
            for (int i = 0; i < cantidadEmpleados; i++)
            {
                Console.WriteLine("Cual es el nombre del empleado?");
                string nombre = Console.ReadLine();
                Console.WriteLine("Cual es el apellido del empleado?");
                string apellido = Console.ReadLine();
                Console.WriteLine("Cual es la edad del empleado?");
                string edad = Console.ReadLine();
                Console.WriteLine("Cual es el sueldo del empleado?");
                string sueldo = Console.ReadLine();
                Console.WriteLine("Cual es el sexo del empleado? (F para femenino y M para masculino)");
                string sexo = Console.ReadLine();

                Empleados empleadosContratados = new Empleados(Convert.ToDouble(sueldo), nombre, apellido, Convert.ToInt32(edad), Convert.ToChar(sexo));
                empleadosContratados.Especializacion = this.categoria;
                empleados.Add(empleadosContratados);
            }

            Console.WriteLine("Se han contratado correctamente");
            return empleados;
        }
        
        public double empleadosContratar()
        {
            Console.WriteLine("¿Cuantos empleados quiere contratar?");
            string contrato = Console.ReadLine();
            return Convert.ToDouble(contrato);
        }

    }
}
