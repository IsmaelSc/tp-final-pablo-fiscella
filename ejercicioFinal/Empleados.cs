﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioFinal
{
    class Empleados : Personas
    {
        protected double sueldo;
        protected string especializacion;
        protected int fechaAlta;

        public Empleados(double sueldo, string nombre, string apellido, int edad, char sexo) : base(nombre, apellido, edad, sexo)
        {
            this.sueldo = sueldo;
        }
        public Empleados(double sueldo, string nombre, string apellido, int edad, char sexo, int fechaAlta) : base(nombre, apellido, edad, sexo)
        {
            this.sueldo = sueldo;
            this.fechaAlta = fechaAlta;
        }
        public String Especializacion
        {
            get
            {
                return this.especializacion;
            }
            set
            {
                this.especializacion = value;
            }
        }
        public String Apellido
        {
            get
            {
                return this.apellido;
            }
            set
            {
                this.apellido = value;
            }
        }
        public double Sueldo
        {
            get
            {
                return this.sueldo;
            }
            set
            {
                this.sueldo = value;
            }
        }
        public int FechaAlta
        {
            get
            {
                return this.fechaAlta;
            }
            set
            {
                this.fechaAlta = value;
            }
        }
    }
}
