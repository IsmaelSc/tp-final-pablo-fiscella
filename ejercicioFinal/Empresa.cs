﻿using EjercicioFinal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicioFinal
{
    class Empresa
    {
        private List<Area> areas = new List<Area>();
        private List<Empleados> empleados_aux = new List<Empleados>();
        private List<Empleados> empleados = new List<Empleados>();
        private List<Empleados> empleadosAntiguos = new List<Empleados>();
        private string textos;
        double gananciasTotales = 0;
        public Empresa(){}

        public double GananciasTotales
        {
            get
            {
                return this.gananciasTotales; 
            }
            set
            {
                this.gananciasTotales = value;
            }
        }

        //CONTRATAR EMPLEADOS
        public List<Empleados> contratarEmpleados()
        {
            Console.Clear();
            Console.WriteLine("¿Cuantas areas quieres contratar?");
            double cantAreas = Convert.ToDouble(Console.ReadLine());
            for (int i = 0; i < cantAreas; i++)
            {
                Console.WriteLine("¿De que area quiere contratar?");
                string tipoTrabajador = Console.ReadLine();
                Area trabajador = new Area(tipoTrabajador);

                empleados_aux = trabajador.agregarEmpleadosCategoria();
                empleados = this.empleadosTotales(empleados_aux);
            }
            return empleados;
        }

        public List<Empleados> empleadosTotales(List<Empleados> empleados_aux)
        {
            for (int i = 0; i < empleados_aux.Count; i++)
            {
                this.empleados.Add(empleados_aux[i]);
            }
            Console.Clear();
            return empleados;
            
        }

        //MOSTRAR EMPLEADOS

        public void mostrarEmpleadosCategoria(List<Empleados> empleadosCategoria)
        {
            Console.Clear();
            if (empleadosCategoria.Count > 0)
            {
                Console.WriteLine("¿De que area quiere mostrar los empleados?");
                string tipoTrabajador = Console.ReadLine();
                for (int i = 0; i < empleadosCategoria.Count; i++)
                {
                    if (empleadosCategoria[i].Especializacion == tipoTrabajador)
                    {
                        Console.WriteLine("El empleado " + empleadosCategoria[i].Apellido + " del area de " + empleadosCategoria[i].Especializacion + "\n");
                    }
                    else
                    {
                        Console.WriteLine(empleadosCategoria[i].Apellido + " no pertenece a esta especializacion");
                    }
                }
            }
            else
            {
                Console.WriteLine("No existe empleados aun");
            }

            
        }


        public void MostrarTodosLosEmpleados(List<Empleados> empleados)
        {
            Console.Clear();
            for (int i = 0; i < empleados.Count; i++)
            {
                Console.WriteLine("El empleado " + empleados[i].Apellido + " del area de " + empleados[i].Especializacion + "\n");
            }
        }

        //Aumentar Salarios

        public void aumentarSalario(string apellido, double montoAumentar, List<Empleados> empleados)
        {
            Console.Clear();
            for (int i = 0; i < empleados.Count; i++)
            {
                if (empleados[i].Apellido == apellido)
                {
                    empleados[i].Sueldo += montoAumentar;
                    Console.WriteLine("Se ha aumentado correctamente el sueldo de {0} es de ahora {1}", empleados[i].Apellido, empleados[i].Sueldo);
                }
               
            }
        }

        public void aumentarSalario(List<Empleados> empleados)
        {
            Console.Clear();
            double aumentoGeneral = 0.5;
            for (int i = 0; i < empleados.Count; i++)
            {
                empleados[i].Sueldo *= aumentoGeneral;
                Console.WriteLine("Se ha aumentado correctamente el sueldo de todos los empleados");
                Console.ReadLine();
            }
        }

        public void aumentarSalario(string area, List<Empleados> empleados)
        {
            Console.Clear();
            double aumentoGeneral = 0.5;
            for (int i = 0; i < empleados.Count; i++)
            {
                if (empleados[i].Especializacion == area)
                {
                    empleados[i].Sueldo *= aumentoGeneral;
                }
                Console.WriteLine("Se ah aunmentado correctamente el sueldo del area {0} de {1} es de ahora {2} \n", empleados[i].Especializacion, empleados[i].Apellido, empleados[i].Sueldo);
                Console.ReadLine();
            }
        }

        public void aumentarSalarioXEdadServicio(int años)
        {
            Console.Clear();
            empleadosAnteriores();

            double aumentoServicio = 0.9;
            for (int i = 0; i < empleadosAntiguos.Count; i++)
            {
                int añosTrabajo = empleadosAntiguos[i].FechaAlta;
                if (añosTrabajo > años)
                {
                    empleadosAntiguos[i].Sueldo *= aumentoServicio;
                    textos += empleadosAntiguos[i].Apellido + "\n";
                }
                else
                {
                    Console.WriteLine("El siguiente empleado no cumple con los años solicitados: {0}", empleadosAntiguos[i].Apellido);
                }
            }
            Console.WriteLine("Se ah aunmentado correctamente el sueldo de los siguientes empleados: \n" + textos);
            Console.ReadLine();
            Console.Clear();
        }
        public void empleadosAnteriores()
        {
            Empleados empleado1 = new Empleados(11.000, "Juan", "Lopez", 43, 'M', 5);
            Empleados empleado2 = new Empleados(5.000, "Lucas", "Martinez", 45, 'M', 4);
            Empleados empleado3 = new Empleados(11.000, "Marila", "Girly", 54, 'F', 3);

            empleadosAntiguos.Add(empleado1);
            empleadosAntiguos.Add(empleado2);
            empleadosAntiguos.Add(empleado3);
        }

        //DESPEDIR EMPLEADOS

        public void despedirEmpleado(string apellido, List<Empleados> empleados)
        {
            Console.Clear();
            for (int i = 0; i < empleados.Count; i++)
            {

                if (empleados[i].Apellido == apellido)
                {

                    Console.WriteLine("Se ha despedido el empledao {0}", empleados[i].Apellido);
                    empleados.RemoveAt(i);
                    Console.ReadLine();
                    
                }
                else
                {

                    Console.WriteLine("No existe el empleado {0} en esta empresa", apellido);
                    Console.ReadLine();
                    Console.Clear();
                }
            }
        }

        //Ganancias Empresa

        public void GananciasEmpresa(double gananciasTotales)
        {
            Console.Clear();
            if (gananciasTotales > 0)
            {
                Console.WriteLine("Las ganancias totales son: {0}", gananciasTotales);
            }
            else
            {
                Console.WriteLine("No se ha generado ganancias aun");
            }
            Console.ReadLine();
            Console.Clear();
        }
    }
}
