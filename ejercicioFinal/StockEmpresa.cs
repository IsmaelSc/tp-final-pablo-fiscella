﻿using EjercicioFinal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ejercicioFinal
{
    class StockEmpresa
    {
        private static List<Producto> productos = new List<Producto>();
        static double gananciasTotales;
        public static List<Producto> AgregarStock()
        {
            Console.WriteLine("¿Que producto quiere aumentar al stock? (computadora o teclado)" );
            string produc = Console.ReadLine();
            Console.WriteLine("¿Cuanto stock quiere aumentar del producto {0}?", produc);
            int cuantoAumentarStock = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < cuantoAumentarStock; i++)
            {
                switch(produc.ToLower())
                {
                    case "teclado":
                        Console.WriteLine("¿Que precio tiene?");
                        double precio = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("¿El nombre de la marca?");
                        string marca = Console.ReadLine();
                        Computadora pcs = new Computadora("Convencional", "intel i7", "Fuente Atx 550w", precio, "computadora", marca, cuantoAumentarStock);
                        productos.Add(pcs);
                        break;
                    case "computadora":
                        Console.WriteLine("¿Que precio tiene?");
                        precio = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("¿El nombre de la marca?");
                        marca = Console.ReadLine();
                        Teclado teclados = new Teclado("Mecanico", "USB", precio, "teclado", marca, cuantoAumentarStock);
                        productos.Add(teclados);
                        break;
                    default:
                        break;

                }
            }
            Console.WriteLine("Se agregaron productos con sus respesctivos stocks");
            Console.ReadLine();
            return productos;
        }

        public static double MostrarProductoParaVender(List<Producto> productos)
        {
            Console.WriteLine("posiciones del array "+productos.Count);

            Console.Clear();
            
            for (int i = 0; i < productos.Count; i++)
            {
                Console.WriteLine("cantidad: " + productos[i].Cantidad);

                if (productos[i].Cantidad >= 1)
                {
                    Console.WriteLine("La marca {0} vale {1}", productos[i].Marca, productos[i].Precio);
                    Console.WriteLine("¿Quieres vender este producto? (si/no)");
                    string res = Console.ReadLine();

                    if (res.ToLower() == "si")
                    {
                        Console.WriteLine("Se ha vendido correctamente");
                        gananciasTotales += productos[i].Precio;
                        productos[i].Vendido = true;


                    }
                }
            }
            Console.ReadLine();
            Console.Clear();
            return gananciasTotales;

        }
    }
}
