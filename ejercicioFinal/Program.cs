﻿using EjercicioFinal;
using System;
using System.Collections.Generic;

namespace ejercicioFinal
{
    class Program
    {
        static bool salir = false;
        static List<Empleados> empleados = new List<Empleados>();
        static List<Producto> productos = new List<Producto>();
        static List<Producto> Newproductos = new List<Producto>();
        static double gananciasTotales = 0;

        static void Main(string[] args)
        {
            while (!salir)
            {
                menu();
            }
        }

        static void menu()
        {

            Console.WriteLine("a. Contratar empleados. \n" +
                "b. Mostrar empleados por área. \n" +
                "c. Mostrar a todos los empleados. \n" +
                "d. Aumentar el salario a un empleado en específico. \n" +
                "e. Aumentar el salario a todos los empleados de un área. \n" +
                "f. Aumentar el salario a todos los empleados. \n" +
                "g. Aumentar el salario a los empleados que tengan más de X años de servicio (La cantidad de años debe poder ser introducida por consola). \n" +
                "h. Despedir un empleado. \n" +
                "i. Aumentar stock de un determinado producto(El producto debe ser elegido dentro de los productos que vende la empresa). \n" +
                "j. Vender un producto: Debe mostrar los diferentes productos que vende la empresa con sus respectivos stocks.No se debe poder vender más productos de los que hay en stock. \n" +
                "k. Calcular ganancias de la empresa.Debe devolver la suma de los valores de todos los productos vendidos hasta el momento.Al iniciar el programa, como aún no se ha vendido nada, debe devolver 0. \n" +
                "l. Salir");

            string resp;
            
            Empresa empresa = new Empresa();
            StockEmpresa Stock = new StockEmpresa();
            ConsoleKeyInfo tecla = Console.ReadKey();

            switch (tecla.KeyChar)
            {

                case 'a':
                    empleados = empresa.contratarEmpleados();
                    break;

                case 'b':
                    empresa.mostrarEmpleadosCategoria(empleados);
                    Console.ReadLine();
                    break;

                case 'c':
                    empresa.MostrarTodosLosEmpleados(empleados);
                    Console.ReadLine();
                    break;

                case 'd':
                    Console.WriteLine("¿A que empleado desea aumentar el salario?(APELLIDO)");
                    string apellido = Console.ReadLine();
                    Console.WriteLine("¿Cuanto deseas aumentar?");
                    double montoAumentar = Convert.ToDouble(Console.ReadLine());
                    empresa.aumentarSalario(apellido, montoAumentar, empleados);
                    Console.ReadLine();
                    break;

                case 'e':
                    Console.WriteLine("¿A que area desea aumentar el salario?");
                    string area = Console.ReadLine();
                    empresa.aumentarSalario(area,empleados);
                    break;


                case 'f':
                    Console.WriteLine("Aumento general a todos en la empresa");
                    empresa.aumentarSalario(empleados);
                    Console.ReadLine();
                    break;

                case 'g':
                    Console.WriteLine("¿A que empleados desea aumentar el salario segun los años de servicio? (AÑOS)");
                    int años = Convert.ToInt32(Console.ReadLine());
                    empresa.aumentarSalarioXEdadServicio(años);
                    break;

                case 'h':
                    Console.WriteLine("");
                    Console.WriteLine("¿A que empleado desea despedir?(APELLIDO)");
                    apellido = Console.ReadLine();
                    empresa.despedirEmpleado(apellido, empleados);
                    break;

                case 'i':

                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("¿Quieres Agregar Al stock ? SI/NO");
                    resp = Console.ReadLine();

                    if (resp.ToLower() == "si" )
                    {
                        productos = StockEmpresa.AgregarStock();
                        Console.Clear();
                    }
                    break;

                case 'j':
                    Newproductos.Clear();
                    for (int i = 0; i < productos.Count; i++)
                    {
                        if (productos[i].Vendido == false)
                        {
                            Newproductos.Add(productos[i]);
                        }

                    }
                    gananciasTotales = StockEmpresa.MostrarProductoParaVender(Newproductos);
                    Console.WriteLine("ganacias:  " + gananciasTotales);
                    break;

                case 'k':
                    empresa.GananciasEmpresa(gananciasTotales);
                    break;

                default:
                    Console.WriteLine("");
                    Console.WriteLine("¿Quieres salir SI/NO?");
                    resp = Console.ReadLine();
                    if (resp.ToLower() == "no")
                    {
                        Console.Clear();
                        break;
                    }
                    else
                    {
                        salir = true;
                        break;
                    }
            }
        }
    }
}
