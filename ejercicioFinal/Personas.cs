﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioFinal
{
    class Personas
    {
        protected string nombre;
        protected string apellido;
        protected int edad;
        protected char sexo;

        public Personas(string nombre, string apellido, int edad, char sexo)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.sexo = sexo;
        }
    }
}
