﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioFinal
{
    class Computadora : Producto
    {
        private string placaMadre;
        private string cpu;
        private string fuenteDeAlimentacion;

        public Computadora(string placaMadre, string cpu, string fuenteDeAlimentacion, double precio, string nombre, string marca, int cantidad) : base(precio, nombre, marca, cantidad)
        {
            this.placaMadre = placaMadre;
            this.cpu = cpu;
            this.fuenteDeAlimentacion = fuenteDeAlimentacion;
        }
    }
}
