﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioFinal
{
    class Teclado : Producto
    {
        private string tipoDeTeclado;
        private string perifericoDeEntrada;
        
        public Teclado(string tipoDeTeclado, string perifericoDeEntrada, double precio, string nombre, string marca, int cantidad) : base(precio, nombre, marca, cantidad)
        {
            this.tipoDeTeclado = tipoDeTeclado;
            this.perifericoDeEntrada = perifericoDeEntrada;
            this.nombre = nombre;
        }

     
    }
}
