﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioFinal
{
    class Producto
    {
        protected double precio;
        protected string nombre;
        protected string marca;
        protected int cantidad;
        private bool vendido = false;


        public Producto(double precio, string nombre, string marca, int cantidad)
        {
            this.precio = precio;
            this.nombre = nombre;
            this.marca = marca;
            this.cantidad = cantidad;
        }
        public double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                this.precio = value;
            }
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                this.nombre = value;
            }
        }
        public string Marca
        {
            get
            {
                return marca;
            }
            set
            {
                this.marca = value;
            }
        }
        public int Cantidad
        {
            get
            {
                return cantidad;
            }
            set
            {
                this.cantidad = value;
            }
        }

        public bool Vendido
        {
            get
            {
                return vendido;
            }
            set
            {
                vendido = value;
            }
        }
    }
}
